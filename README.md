# IT Help Desk Org

## Part II - Email Service

## Overview

Email service that creates IT Help Desk Tickets from incoming emails. It also allows the sender to update an existing ticket via email if they include the case number in the subject line. Users receive a confirmation email both when their ticket is created or updated.

## Quick Demo (~90 Sec)

[Here's a link to a demo video for the service](https://www.youtube.com/watch?v=dUqqlZ5jeps)

## File Overview

### Apex

* **TicketEmailService.cls** -  Implements Messaging.InboundEmailHandler and handles inbound emails; sends update emails to users

* **TicketDataService.cls** - Handles database interactions for Tickets; used by both email service and VF controller.

* **OutboundEmailUtils.cls** - Helper for email service that builds strings used in outbound emails.

* **Constants.cls** - Retreives custom metadata values used by the email and data services

## Visual Overview

### Context

![Context Diagram](media/diagrams/context.png)

### Container

![Container Diagram](media/diagrams/container.png)

## Part I - Visualforce Page

## Overview

Visualforce page for creating new Help Desk tickets and assigning them to a Help Desk Queue. User enters an employee Id and the page displays the employee's information. User then fills in the ticket's details and submits it to the Help Desk Queue. Previous tickets associated with the employee are also displayed at the bottom of the page for reference.

## Quick Demo

### Creating a Ticket

![Creating a ticket](media/create_case.gif)

### Viewing Previous Tickets

![Viewing user's previous tickets](media/view_previous_tickets.gif)

### Overriding New Case Button

![Overriding default New Case button with VF page](media/override_new_button.gif)

## File Overview

### Apex

* **HelpDeskTicketPageControllerExtension.cls** - Visualforce page controller. Used to insert Case, manage page messages, and navigate to record detail pages.

* **HelpDeskTicketPageController_Test.cls** - Test class for page controller.

### Page

* **HelpDeskTicketPage.page** - Visualforce page with an input for employee Id searches, inputs for ticket details/submission, and a display for previous cases associated with found employee.

* **EmployeeInfoDisplay.component** - Component used in page for displaying employee info. Uses AJAX toolkit to query for a user based on passed Employee Id, then displays record field values on page (First Name, Last Name, Supervisor, etc). Also queries for previous cases associated with found user and passes it to the controller.

### Static Resources

* **HelpDeskTicketPageCSS.css** - CSS file used for page styling.