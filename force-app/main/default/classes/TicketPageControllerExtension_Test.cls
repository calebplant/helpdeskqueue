@isTest
public with sharing class TicketPageControllerExtension_Test {

    private static final String STANDARD_USER_EMPLOYEE_ID = '9999999999';
    private static final String STANDARD_USER_DESIGNATION ='A designation goes here';
    private static final String SUPPORT_EMPLOYEE_EMPLOYEE_ID = '8888888888';
    private static final String ACCOUNT_ONE_NAME = 'Apples';

    @TestSetup
    static void makeData(){

        // Standard user (employee)
        Profile standardProf = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User standardUser = new User(Alias = 'standt', Email='someemployee@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='StandardTesting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = standardProf.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='someemployee@testorg.com',
            Employee_Id__c=STANDARD_USER_EMPLOYEE_ID, Designation__c=STANDARD_USER_DESIGNATION);
        insert standardUser;

        // Support user
        Profile supportProfile = [SELECT Id FROM Profile WHERE Name='My IT Support'];
        UserRole supportRole = [SELECT Id FROM UserRole WHERE Name='L1 Support'];
        User supportUser = new User(Alias = 'standt', Email='somesupport@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='SupportTesting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = supportProfile.Id, UserRoleId=supportRole.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName='somesupport@testorg.com',
            Employee_Id__c = SUPPORT_EMPLOYEE_EMPLOYEE_ID);
        insert supportUser;
        PermissionSet viewAllUsers = [SELECT Id FROM PermissionSet WHERE Name = 'View_All_Users'];
        insert new PermissionSetAssignment(AssigneeId = supportUser.id, PermissionSetId = viewAllUsers.Id);

    }

    @isTest
    static void doesCreateNewTicketInsertCaseOnSuccess()
    {  
        String employeeOneRecordId = [SELECT Id, Employee_Id__c FROM User WHERE Employee_Id__c=:STANDARD_USER_EMPLOYEE_ID].Id;
        PageReference myVfPage = Page.HelpDeskTicketPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);
        myController.employeeId = employeeOneRecordId;

        Test.startTest();
        PageReference finishPage = myController.createNewTicket();
        Test.stopTest();

        List<Case> cases = [SELECT Id FROM Case];
        System.assertEquals(1, cases.size(), 'New case was NOT inserted.');
        System.assertNotEquals(ApexPages.currentPage(), finishPage, 'Page was not redirected after successful insert.');
    }

    @isTest
    static void doesCreateNewTicketDisplayErrorOnInsertError()
    {
        String employeeOneRecordId = [SELECT Id, Employee_Id__c FROM User WHERE Employee_Id__c=:STANDARD_USER_EMPLOYEE_ID].Id;
        // Get an Account Id
        Account acc = new Account(Name=ACCOUNT_ONE_NAME);
        insert acc;

        PageReference myVfPage = Page.HelpDeskTicketPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);
        myController.employeeId = employeeOneRecordId;

        Test.startTest();
        // force save result error
        // myController.ticketCase.AccountId = UserInfo.getUserId();
        myController.employeeId = null;
        myController.previousTickets = new List<Case>{new Case(Complainant__c=acc.Id)}; 
        PageReference finishPage = myController.createNewTicket();
        Test.stopTest();

        List<Case> cases = [SELECT Id FROM Case];
        System.assertEquals(0, cases.size(), 'New case was inserted.');
        System.assertEquals(null, finishPage, 'Page was redirected after UNSUCCESSFUL insert.');
    }
    
    @isTest
    static void doesAddPageMessageAddCustomMessage()
    {
        PageReference myVfPage = Page.HelpDeskTicketPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);

        Test.startTest();
        myVfPage.getParameters().put('severity', 'INFO');
        myVfPage.getParameters().put('message', 'message1');
        myController.addPageMessage();
        myVfPage.getParameters().put('severity', 'ERROR');
        myVfPage.getParameters().put('message', 'message2');
        myController.addPageMessage();
        myVfPage.getParameters().put('severity', 'WARNING');
        myVfPage.getParameters().put('message', 'message3');
        myController.addPageMessage();
        Test.stopTest();

        System.assertEquals(3, ApexPages.getMessages().size(), 'Not all page messages were successfully added.');
    }

    @isTest
    static void doesSetFoundEmployeeIdSetEmployeeId()
    {
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);
        myController.employeeId = '0051F00000kmIL2QAM';

        Test.startTest();
        myController.setFoundEmployeeId();
        Test.stopTest();

        System.assertEquals('0051F00000kmIL2QAM', myController.employeeId, 'Controller did not set employeeId');
    }

    @isTest
    static void doesClearPreviousTicketsClearPreviousTicketData()
    {
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);
        List<Case> previousTicketData = new List<Case>();
        previousTicketData.add(new Case());
        previousTicketData.add(new Case());
        myController.previousTickets = previousTicketData;

        System.assertEquals(2, myController.previousTickets.size(), 'Did not start test with correct number of existing tickets.');
        Test.startTest();
        myController.clearPreviousTickets();
        Test.stopTest();

        System.assertEquals(0, myController.previousTickets.size(), 'Did NOT clear existing ticket data.');
    }

    @isTest
    static void doesSetFoundPreviousTicketsUpdateWhenEmployeeHasMultiplePreviousTickets()
    {
        PageReference myVfPage = Page.HelpDeskTicketPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);

        List<Case> previousTicketData = new List<Case>();
        previousTicketData.add(new Case(Status='New'));
        previousTicketData.add(new Case(Status='Working'));
        System.assertEquals(2, previousTicketData.size(), 'Did not start test with correct number of existing tickets.');
        String previousTicketJson = JSON.serialize(previousTicketData);
        System.debug('previousTicketJson: ' + previousTicketJson);

        Test.startTest();
        myVfPage.getParameters().put('previousTickets', previousTicketJson);
        myVfPage.getParameters().put('hasMultiplePreviousTickets', 'true');
        myController.setFoundPreviousTickets();
        Test.stopTest();

        System.assertEquals(2, myController.previousTickets.size(), 'Controller did not set previousTickets.');
    }

    @isTest
    static void doesSetFoundPreviousTicketsUpdateWhenEmployeeHasSinglePreviousTicket()
    {
        PageReference myVfPage = Page.HelpDeskTicketPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);

        Case previousTicketData = new Case(Status='New');
        String previousTicketJson = JSON.serialize(previousTicketData);

        Test.startTest();
        myVfPage.getParameters().put('previousTickets', previousTicketJson);
        myVfPage.getParameters().put('hasMultiplePreviousTickets', 'false');
        myController.setFoundPreviousTickets();
        Test.stopTest();

        System.assertEquals(1, myController.previousTickets.size(), 'Controller did not set previousTickets.');
    }

    @isTest
    static void doesNavigateToTicketRedirectIfRecordExists()
    {
        Case newCase = new Case(Status='New', Description='myDesc');
        insert newCase;
        PageReference myVfPage = Page.HelpDeskTicketPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);

        Test.startTest();
        myVfPage.getParameters().put('ticketRecordId', newCase.Id);
        PageReference finishPage = myController.navigateToTicket();
        Test.stopTest();

        System.assertNotEquals(ApexPages.currentPage(), finishPage, 'Did not successfully redirect to new page.');
    }

    @isTest
    static void doesNavigateToTicketReturnNullIfRecordDoesntExist()
    {
        PageReference myVfPage = Page.HelpDeskTicketPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);
        Id nonexistantCaseId = '5001F000005s21mQAA';

        Test.startTest();
        myVfPage.getParameters().put('ticketRecordId', nonexistantCaseId);
        PageReference finishPage = myController.navigateToTicket();
        Test.stopTest();

        System.assertEquals(null, finishPage, 'Redirected page on UNSUCCESSFUL case insert.');
    }

    /* ApexPages.getMessages().clear() does not work inside a test. */
    @isTest
    static void doesClearPageMessagesRemovePageMessages()
    {
        PageReference myVfPage = Page.HelpDeskTicketPage;
        Test.setCurrentPage(myVfPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(new Case());
        TicketPageControllerExtension myController = new TicketPageControllerExtension(sc);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'a new message'));

        System.debug('messages:' + ApexPages.getMessages());

        System.assertEquals(1, ApexPages.getMessages().size(), 'Test did not start with correct number of existing page messages.');
        Test.startTest();
        myController.clearPageMessages();
        Test.stopTest();
    }

    /*
    @isTest
    static void doesSupportUserHaveAccessToEmployees()
    {
        User supportUser = [SELECT Id FROM User WHERE Employee_Id__c=:SUPPORT_EMPLOYEE_EMPLOYEE_ID];
        System.runas(supportUser) {
            List<User> standardUserEmployee = [SELECT Id, Employee_Id__c FROM User WHERE Employee_Id__c=:STANDARD_USER_EMPLOYEE_ID];
            System.assertEquals(1, standardUserEmployee.size(), 'Support user could not view existing standard user.');
        }
        User standardUser = [SELECT Id FROM User WHERE Employee_Id__c=:STANDARD_USER_EMPLOYEE_ID];
        System.runas(standardUser) {
            List<User> supportUserEmployee = [SELECT Id, Employee_Id__c FROM User WHERE Employee_Id__c=:SUPPORT_EMPLOYEE_EMPLOYEE_ID];
            System.assertEquals(0, supportUserEmployee.size(), 'Standard user could view support user.');
        }
    }
    */
    
}
