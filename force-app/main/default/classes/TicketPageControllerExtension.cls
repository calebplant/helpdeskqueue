public with sharing class TicketPageControllerExtension {
    // public User employee {get; set;}
    public String searchId {get; set;}
    public Case ticketCase {get; set;}
    public List<Case> previousTickets {get; set;}
    public Id employeeId {get; set;}

    public TicketPageControllerExtension(ApexPages.StandardController stdController)
    {
        System.debug('Calling constructor');
        ticketCase = new Case();
        previousTickets = new List<Case>();
    }

    public PageReference createNewTicket()
    {
        System.debug('START createNewTicket');
        System.debug('prev tickets');
        System.debug(previousTickets);

        String insertResult = TicketDataService.doInsert(ticketCase, employeeId, previousTickets);
        if(!insertResult.startsWith('ERRORS')) {
            TicketEmailService.sendEmailToComplainant(insertResult, employeeId, previousTickets, TicketOperationType.CREATED);
            return new Pagereference('/' + insertResult);
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, insertResult));
        }
        return null;
    }

    public void addPageMessage()
    {
        System.debug('START addApexMessage');
        String customMessage = ApexPages.currentPage().getParameters().get('message');
        String severity = ApexPages.currentPage().getParameters().get('severity');

        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, '');
        if(severity == 'ERROR') {
            myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, customMessage);
        } else if(severity == 'WARNING') {
            myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, customMessage);
        } else if (severity == 'INFO') {
            myMsg = new ApexPages.Message(ApexPages.Severity.INFO, customMessage);
        }
        ApexPages.addMessage(myMsg);
    }

    public void clearPageMessages()
    {
        System.debug('START clearPageMessages');
        ApexPages.getMessages().clear();
    }

    public void setFoundEmployeeId()
    {
        System.debug('Setting employeeId . . .');
        System.debug(employeeId);
    }

    public PageReference clearPreviousTickets()
    {
        System.debug('clearPreviousTickets');
        previousTickets = new List<Case>();
        return null;
    }

    public PageReference setFoundPreviousTickets()
    {
        System.debug('Setting previous tickets . . .');
        String previousTicketsData = ApexPages.currentPage().getParameters().get('previousTickets');
        String hasMultiplePreviousTickets = ApexPages.currentPage().getParameters().get('hasMultiplePreviousTickets');
        Boolean multipleTickets = hasMultiplePreviousTickets == 'true' ? true : false;
        if(multipleTickets) {
            previousTickets = (List<Case>)JSON.deserialize(previousTicketsData, List<Case>.class);
        } else {
            previousTickets = new List<Case>();
            previousTickets.add((Case)JSON.deserialize(previousTicketsData, Case.class));
        }
        System.debug('previousTickets:');
        System.debug(previousTickets);

        return null;
    }

    public PageReference navigateToTicket()
    {
        System.debug('navigateToTicket');
        String ticketRecordId = ApexPages.currentPage().getParameters().get('ticketRecordId');
        List<Case> foundCases = [SELECT Id FROM Case WHERE Id =: ticketRecordId WITH SECURITY_ENFORCED];
        System.debug(foundCases);
        if(foundCases.size() > 0) {
            System.debug('Navigating to: ' + ticketRecordId);
            return new PageReference('/' + ticketRecordId);
        } else {
            System.debug('No record found with Id ' + ticketRecordId + '. Staying on page.');
            return null;
        }
    }
}
