public with sharing class OutboundEmailUtils {

    public class HelpDeskTickets {
        public Id getHelpDeskSenderId()
        {
            List<OrgWideEmailAddress> owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :Constants.getHelpDeskOrgEmail()];
            if ( owea.size() > 0 ) {
                return owea.get(0).Id;
            } else {
                return null;
            }
        }
        

        public String buildCreatedSubject(String caseSubject)
        {
            String subject = 'Ticket Created Re: ' + caseSubject;
            return subject;
        }


        public String buildCreatedHtmlBody(String complainantName, String caseNumber, String caseSubject)
        {
            String body = 'Dear ' + complainantName + ',<br><br>' +
                'The help desk has received your ticket and is now working to solve your problem as soon as possible.<br><br>' +
                '<b>Case Number:</b> ' + caseNumber + '<br>' +
                '<b>Case Subject:</b> ' + caseSubject + '<br><br>' +
                'If you have any questions, please contact the Help Desk and reference the above Case Number.<br>' +
                'If you would like to add any information regarding this case, please send an email to the help desk ' +
                'and include the above Case Number in the subject line.<br><br>' +
                'Thank you,<br>' + 
                'The Help Desk Team';
            return body;
        }

        public String buildUpdatedSubject(String caseSubject)
        {
            String subject = 'Ticket Updated Re: ' + caseSubject;
            return subject;
        }


        public String buildUpdatedHtmlBody(String complainantName, String caseNumber, String caseSubject)
        {
            String body = 'Dear ' + complainantName + ',<br><br>' +
                'The help desk has updated your ticket and is continuing work to resolve your issue as soon as possible.<br><br>' +
                '<b>Case Number:</b> ' + caseNumber + '<br>' +
                '<b>Case Subject:</b> ' + caseSubject + '<br><br>' +

                'Thank you,<br>' + 
                'The Help Desk Team';
            return body;
        }

        public String getErrorBody(String exceptionMessage) {
            switch on exceptionMessage {
                when 'List has no rows for assignment to SObject' {
                    return 'No user was found associated with the sending email. Have you created your account? If so, please' +
                            'contact the help desk at XXX-XXX-XXXX.';
                }
                when else {
                    return 'An an unknown error occurred while processing your request.\n' +
                            'Please contact the help desk at XXX-XXX-XXXX.';
                }
            }
        }
    }
}
