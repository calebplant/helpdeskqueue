@isTest
public class TicketEmailService_Tests {

    private static final String EXISTING_USER_NAME = 'Steve Testerman';
    private static final String NEW_CASE_SUBJECT = 'A Case Subject Goes Here';
    private static final String NON_EXISTANT_EMAIL = 'ojuabsd@okad.com';
    private static final String EMAIL_BODY = 'Hi,\n\nI bought a banana at your store a week ago and its no longer yellow? Its brown. Please send another!\n\nThanks,\nSteve';
    private static final String EMAIL_SUBJECT_NO_CASE_NUMBER = 'Banana No Longer Yellow?';

    @TestSetup
    static void makeData(){
        User existingUser = [SELECT Id FROM User
                            WHERE Name = :EXISTING_USER_NAME];
        List<Case> cases = new List<Case>();
        cases.add(new Case(Complainant__c=existingUser.Id, Subject=NEW_CASE_SUBJECT));
        insert cases;
    }

    @isTest
    static void doesHandleInboundEmailCreateCaseOnValidInput()
    {
        User existingUser = [SELECT Id, Name, Email FROM User
                            WHERE Name = :EXISTING_USER_NAME];
        Integer startSize = [SELECT COUNT() FROM Case];

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();

        email.Subject = EMAIL_SUBJECT_NO_CASE_NUMBER;
        email.fromAddress = existingUser.Email;
        email.plainTextBody = EMAIL_BODY;

        TicketEmailService testInbound = new TicketEmailService();
        Test.startTest();
        Messaging.InboundEmailResult emailResult = testInbound.handleInboundEmail(email, envelope);
        Test.stopTest();

        Integer endSize = [SELECT COUNT() FROM Case];
        // System.debug('Start: ' + startSize + '  End: ' + endSize);
        System.assertEquals(startSize + 1, endsize, 'New case was NOT created.');
        System.assertEquals(true, emailResult.success, 'Inbound email not successfully processed.');
        System.assertEquals(null, emailResult.message, 'Inbound email generated error message.');
    }

    @isTest
    static void doesHandleInboundEmailFailToInsertCaseOnUnknownEmail()
    {
        Integer startSize = [SELECT COUNT() FROM Case];

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();

        email.Subject = EMAIL_SUBJECT_NO_CASE_NUMBER;
        email.fromAddress = NON_EXISTANT_EMAIL;
        email.plainTextBody = EMAIL_BODY;

        TicketEmailService testInbound = new TicketEmailService();
        Test.startTest();
        Messaging.InboundEmailResult emailResult = testInbound.handleInboundEmail(email, envelope);
        Test.stopTest();

        Integer endSize = [SELECT COUNT() FROM Case];
        System.assertEquals(startSize, endsize, 'New case was created.');
        System.assertEquals(false, emailResult.success, 'Inbound email successfully processed.');
        System.assertNotEquals(null, emailResult.message, 'Inbound email did NOT generate error message.');
    }

    @isTest
    static void doesHandleInboundEmailUpdateExistingCase()
    {
        User existingUser = [SELECT Id, Name, Email FROM User
                            WHERE Name = :EXISTING_USER_NAME];
        Case existingCase = [SELECT Id, CaseNumber, Description FROM Case WHERE Complainant__c = :existingUser.Id];
        String targetBodyString = 'Mississippi';

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();

        email.Subject = 'Regarding ' + existingCase.CaseNumber;
        email.fromAddress = existingUser.Email;
        email.plainTextBody = targetBodyString;

        TicketEmailService testInbound = new TicketEmailService();
        Test.startTest();
        Messaging.InboundEmailResult emailResult = testInbound.handleInboundEmail(email, envelope);
        Test.stopTest();

        existingCase = [SELECT Id, Description FROM Case WHERE Complainant__c = :existingUser.Id];
        System.assert(existingCase.Description.contains(targetBodyString), 'Inbound email body was not added to existing case description.');
        System.assertEquals(true, emailResult.success, 'Inbound email NOT successfully processed.');
        System.assertEquals(null, emailResult.message, 'Inbound email generated error message.');
    }
}
