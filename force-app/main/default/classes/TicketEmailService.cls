global without sharing class TicketEmailService implements Messaging.InboundEmailHandler{

    private static OutboundEmailUtils.HelpDeskTickets ticketEmailUtils = new OutboundEmailUtils.HelpDeskTickets();
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {

        // Declarations
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        result.success = false;
        TicketOperationType emailType = null;
        User sender = new User();
        Case existingCase = null;
        Case resultCase = new Case();

         // Get User from sender's email
        try {
            sender = [SELECT Id, Name, Email FROM User
                            WHERE Email = :email.fromAddress
                            WITH SECURITY_ENFORCED
                            LIMIT 1];
        } catch(QueryException e) {
            System.debug('Query Issue: ' + e);
            result.message = ticketEmailUtils.getErrorBody(e.getMessage());
            return result;
        }

        // Create/Update ticket
        String caseNumber = getCaseNumberFromSubject(email.subject);
        if(caseNumber != null) {
            existingCase = TicketDataService.getExistingCase(caseNumber, sender.Id);
        }

        if(existingCase != null) { // Update
            Case updatedCase = TicketDataService.doUpdate(existingCase, email);
            if(updatedCase != null) {
                resultCase = updatedCase;
                emailType = TicketOperationType.UPDATED;
            }
        } else { // Create
            Case insertedCase = TicketDataService.doInsert(email, sender);
            if(insertedCase != null) {
                resultCase = insertedCase;
                emailType = TicketOperationType.CREATED;
            }
        }

        // Send outbound email
        if(emailType != null) {
            sendEmailToComplainant(resultCase, sender, emailType);
            result.success = true;
        }

        // Check if unknown error occurred
        if(!result.success && result.message == null) {
            result.message = ticketEmailUtils.getErrorBody('');
        }

        return result;
    }

    public static void sendEmailToComplainant(Case targetCase, User sender, TicketOperationType ticketType)
    {
        System.debug('START sendEmailToComplainant');
        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage outboundEmail = new Messaging.SingleEmailMessage();
        Id helpDeskSenderId = ticketEmailUtils.getHelpDeskSenderId();

        // Outbound email settings
        outboundEmail.setTargetObjectId(sender.Id);
        outboundEmail.setTreatTargetObjectAsRecipient(true);
        outboundEmail.setSaveAsActivity(false);
        if(helpDeskSenderId != null) {
            outboundEmail.setOrgWideEmailAddressId(helpDeskSenderId);
        }
        // Subject/Body
        switch on ticketType {
            when CREATED {
                outboundEmail.setSubject(ticketEmailUtils.buildCreatedSubject(targetCase.subject));
                outboundEmail.setHtmlBody(ticketEmailUtils.buildCreatedHtmlBody(sender.Name, targetCase.CaseNumber, targetCase.Subject));
            } when UPDATED {
                outboundEmail.setSubject(ticketEmailUtils.buildUpdatedSubject(targetCase.subject));
                outboundEmail.setHtmlBody(ticketEmailUtils.buildUpdatedHtmlBody(sender.Name, targetCase.CaseNumber, targetCase.Subject));
            }
        }

        outboundEmails.add(outboundEmail);
        System.debug('Sending Email: ' + outboundEmails);
        Messaging.sendEmail(outboundEmails);
    }

    public static void sendEmailToComplainant(Id caseId, Id employeeId, List<Case> previousTickets, TicketOperationType operationType)
    {
        System.debug('employeeId: ' + employeeId);
        if(employeeId == null && previousTickets.size() > 0) {
            employeeId = previousTickets[0].Complainant__c;
        }
        Case targetCase = [SELECT Id, CaseNumber, Subject FROM Case
                            WHERE Id = :caseId
                            LIMIT 1];
        User complainant = [SELECT Id, Name FROM User
                            WHERE Id = :employeeId
                            LIMIT 1];
        if(targetCase.Subject == null) {
            targetCase.Subject = targetCase.CaseNumber;
        }
        System.debug('targetCase: ' + targetCase);
        System.debug('complainant' + complainant);
        sendEmailToComplainant(targetCase, complainant, operationType);
    }

    private static String getCaseNumberFromSubject(String emailSubject)
    {
        String caseNumber;

        Pattern pattern = Pattern.compile('\\d{8}+');
        Matcher matcher = pattern.matcher(emailSubject);
        while(matcher.find()){
            caseNumber = matcher.group();
        }
        return caseNumber;
    }
}
