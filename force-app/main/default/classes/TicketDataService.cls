public with sharing class TicketDataService {

    public static Case getNewCaseAsTicketType()
    {
        Case newCase = new Case();
        newCase = setCaseAsTicketType(newCase);
        return newCase;
    }


    public static Case setCaseAsTicketType(Case ticketCase)
    {
        // Get IT Ticket record type Id
        String ticketRecordTypeName = Constants.getTicketRecordTypeName();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName()
                      .get(ticketRecordTypeName).getRecordTypeId();
        // Assign active Case assignment rule to dml options
        AssignmentRule assignmentRule = [select Id from AssignmentRule where SobjectType = 'Case' and Active = True limit 1];
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId = assignmentRule.Id;

        ticketCase.RecordTypeId = recordTypeId;
        ticketCase.setOptions(dmlOpts);
        return ticketCase;
    }

    
    public static Case getExistingCase(String caseNumber, Id senderId) {
        List<Case> foundCase = [SELECT Id, Subject, Complainant__c, CaseNumber, Description FROM Case
                                WHERE CaseNumber = :caseNumber
                                AND Complainant__c = :senderId
                                WITH SECURITY_ENFORCED];
        if(foundCase.size() > 0) {
            System.debug('Existing Case: ' + foundCase[0]);
            return foundCase[0];
        }
        
        return null;
    }

    public static String doInsert(Case newCase, Id employeeId, List<Case> previousTickets)
    {
        Case ticketCase = setCaseAsTicketType(newCase);
        
        // Set RecordType/Origin/Complainant
        ticketCase.Origin = 'Phone';
        if(employeeId == null && previousTickets.size() > 0) {
            ticketCase.Complainant__c = previousTickets[0].Complainant__c;
        } else {
            ticketCase.Complainant__c = employeeId;
        }
        System.debug('ticketCase: ' + ticketCase);
        
        // Insert
        Database.SaveResult sr = Database.insert(ticketCase, false);
        if(sr.isSuccess()) {
            System.debug('Successful insert!' + ticketCase);
            // Navigate to inserted record detail page
            return ticketCase.Id;
        } else {
            String errorMsg = 'ERRORS\n';
            for(Database.Error eachError : sr.getErrors()) {
                errorMsg += eachError.getStatusCode() + ': ' + eachError.getMessage();
                System.debug(eachError.getStatusCode() + ': ' + eachError.getMessage());
            }
            return errorMsg;
        }
    }

    public static Case doInsert(Messaging.InboundEmail email, User sender)
    {
        Case resultCase = new Case();
        Case newTicket = populateNewHelpTicketFieldsFromEmail(email, sender);

        Database.SaveResult sr = Database.insert(newTicket, false);
        if(sr.isSuccess()) {
            System.debug('Successful insert!' + newTicket);
            // Requery to get CaseNumber
            resultCase = [SELECT Id, Subject, Complainant__c, CaseNumber
                        FROM Case
                        WHERE Id = :newTicket.Id];
            return resultCase;
        }
        return null;
    }

    public static Case doUpdate(Case existingCase, Messaging.InboundEmail email)
    {
        List<Case> resultCase = new List<Case>();
        resultCase.add(updateExistingTicketFields(existingCase, email));

        if(Schema.sObjectType.Case.isUpdateable()) {
            SObjectAccessDecision updateDecision = Security.stripInaccessible(AccessType.UPDATABLE, resultCase);
            List<Database.SaveResult> updateResult = Database.Update(updateDecision.getRecords());
            return existingCase;
        }

        return null;
    }

    private static Case updateExistingTicketFields(Case existingCase, Messaging.InboundEmail email)
    {
        existingCase.Description = updateExistingTicketDescription(existingCase.Description, email.plainTextBody);
        return existingCase;
    }

    private static String updateExistingTicketDescription(String existingDescription, String newContent)
    {
        String updateTimestamp = System.now().format('yyyy/MM/dd hh:mm a');
        return existingDescription + '\n\n(' + updateTimestamp + ')\n' + newContent;
    }

    private static Case populateNewHelpTicketFieldsFromEmail(Messaging.InboundEmail email, User sender) {
        Case newCase = getNewCaseAsTicketType();
        newCase.Origin = 'Email';
        newCase.Priority = 'Normal';
        newCase.Status = 'New';
        newCase.Description = email.plainTextBody;
        newCase.Subject = email.subject;
        newCase.Complainant__c = sender.Id;
        return newCase;
    }
}
