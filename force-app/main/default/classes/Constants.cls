public with sharing class Constants {
    
    public static String getTicketRecordTypeName()
    {
        try {
            Ticket_Setting__mdt ticketSettings = [SELECT Ticket_Record_Type_Name__c FROM Ticket_Setting__mdt
                                                WHERE Label = 'Default'
                                                LIMIT 1];
            return ticketSettings.Ticket_Record_Type_Name__c;
        } catch(QueryException e) {
            System.debug('No valid label passed. Check your custom metadata types!');
            return null;
        }
    }

    public static String getHelpDeskOrgEmail()
    {
        try {
            Ticket_Setting__mdt ticketSettings = [SELECT Help_Desk_Org_Email__c FROM Ticket_Setting__mdt
                                                WHERE Label = 'Default'
                                                LIMIT 1];
            return ticketSettings.Help_Desk_Org_Email__c;
        } catch(QueryException e) {
            System.debug('No valid label passed. Check your custom metadata types!');
            return null;
        }
    }
}
